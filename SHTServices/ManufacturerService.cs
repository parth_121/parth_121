﻿using Microsoft.EntityFrameworkCore;
using SHTData;
using SHTData.Models;
using System.Collections.Generic;
using System.Linq;

namespace SHTServices
{
    public class ManufacturerService : IManufacturer
    {
        private SHTContext _context;

        public ManufacturerService(SHTContext context)
        {
            _context = context;
        }

        public void Add(Manufacturer newManufacturer)
        {
            _context.Add(newManufacturer);
            _context.SaveChanges();
        }

        public bool Update(Manufacturer editManufacturer)
        {
            try
            {
                _context.Update(editManufacturer);
                _context.SaveChanges();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
                throw;
            }
        }

        public Manufacturer GetById(int id)
        {
            return _context.Manufacturers
                   .FirstOrDefault(a => a.Id == id);
        }

        public IEnumerable<Manufacturer> GetAll()
        {
            return _context.Manufacturers;
        }

    }
}
