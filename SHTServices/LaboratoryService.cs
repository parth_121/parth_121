﻿using Microsoft.EntityFrameworkCore;
using SHTData;
using SHTData.Models;
using System.Collections.Generic;
using System.Linq;

namespace SHTServices
{
    public class LaboratoryService : ILaboratory
    {
        private SHTContext _context;

        public LaboratoryService(SHTContext context)
        {
            _context = context;
        }

        public void Add(Laboratory newLaboratory)
        {
            _context.Add(newLaboratory);
            _context.SaveChanges();
        }

        public bool Update(Laboratory editLaboratory)
        {
            try
            {
                _context.Update(editLaboratory);
                _context.SaveChanges();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
                throw;
            }
        }

        public IEnumerable<Laboratory> GetAll()
        {
            return _context.Laboratories;
        }

        public Laboratory GetById(int id)
        {
            return _context.Laboratories
               .FirstOrDefault(product => product.Id == id);
        }
    }
}
