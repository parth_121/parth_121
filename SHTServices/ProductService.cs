﻿using Microsoft.EntityFrameworkCore;
using SHTData;
using SHTData.Models;
using System.Collections.Generic;
using System.Linq;

namespace SHTServices
{
    public class ProductService : IProduct
    {
        private SHTContext _context;

        public ProductService(SHTContext context)
        {
            _context = context;
        }

        public bool Add(Product newProduct, Client client)
        {
            try
            {
                _context.Add(newProduct);
                _context.Clients.Update(client);
                _context.SaveChanges();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
                throw;
            }

        }

        public bool Update(Product editProduct)
        {
            try
            {
                _context.Update(editProduct);
                _context.SaveChanges();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
                throw;
            }
        }

        public IEnumerable<Product> GetAll()
        {
            return _context.Products
                .Include(p => p.Manufacturer);
        }

        public IEnumerable<Product> GetAllNew()
        {
            return _context.Products.Where(a => a.ProjectStatus == "Granted" || a.ProjectStatus == "Project Done")
                .Include(p => p.Manufacturer);
        }

        public Product GetById(int? id)
        {
            return _context.Products
                .Include(p => p.Manufacturer)
                .Include(p => p.Laboratory)
                .FirstOrDefault(product => product.Id == id);
        }

        public IEnumerable<Product> GetByManufacturerId(int id)
        {
            var result = _context.Products.Where(p => p.Manufacturer.Id == id);
            return result;
        }
    }
}
