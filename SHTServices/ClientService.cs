﻿using Microsoft.EntityFrameworkCore;
using SHTData;
using SHTData.Models;
using SHTServices;
using System.Collections.Generic;
using System.Linq;

namespace SHTServices
{
    public class ClientService : IClient
    {
        private SHTContext _context;

        public ClientService(SHTContext context)
        {
            _context = context;
        }

        public void Add(Client newClient)
        {
            _context.Add(newClient);
            _context.SaveChanges();

        }

        public IEnumerable<Client> GetAll() => _context.Clients
                .Include(client => client.Products);

        public Client GetById(int? id) => _context.Clients
                .Include(client => client.Products)
                .FirstOrDefault(a => a.Id == id);
    }
}
