using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using SHTData;
using SHTData.Models;

namespace ShannonTech.Controllers
{
    public class ModelsController : BaseController
    {
        private SHTContext _context;
        private IProduct _products;

        public ModelsController(IProduct products, SHTContext context)
        {
            _products = products;
            _context = context;
        }

        //// GET: Models
        //public async Task<IActionResult> Index()
        //{
        //    return View(await _context.Models.ToListAsync());
        //}

        //// GET: Models/Details/5
        //public async Task<IActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var model = await _context.Models
        //        .SingleOrDefaultAsync(m => m.Id == id);
        //    if (model == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(model);
        //}


        //// GET: Models/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var model = await _context.Models.SingleOrDefaultAsync(m => m.Id == id);
        //    if (model == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(model);
        //}

        // POST: Models/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("Id,ModelNumber,TestReportNumber,TestReportReceivedDate,Description")] Model model)
        //{
        //    if (id != model.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(model);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!ModelExists(model.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction("Index");
        //    }
        //    return View(model);
        //}

        //// GET: Models/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var model = await _context.Models
        //        .SingleOrDefaultAsync(m => m.Id == id);
        //    if (model == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(model);
        //}

        //// POST: Models/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var model = await _context.Models.SingleOrDefaultAsync(m => m.Id == id);
        //    _context.Models.Remove(model);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        //private bool ModelExists(int id)
        //{
        //    return _context.Models.Any(e => e.Id == id);
        //}
    }
}
