using Microsoft.AspNetCore.Mvc;
using SHTData;
using SHTData.Models;
using System.Threading.Tasks;

namespace ShannonTech.Controllers
{
    public class ProjectsController : BaseController
    {
        private IProduct _products;
        private IManufacturer _manufacturers;
        private ILaboratory _laboratories;
        private IClient _clients;
        public ProjectsController(IProduct products, IManufacturer manufacturers, ILaboratory laboratories, IClient clients)
        {
            _products = products;
            _manufacturers = manufacturers;
            _laboratories = laboratories;
            _clients = clients;

        }

        // GET: Projects
        public IActionResult Index()
        {
            return View(_products.GetAll());
        }

        // GET: Projects/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = _products.GetById(id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // GET: Projects/Create
        public IActionResult Create()
        {
            ViewData["Manufacturers"] = _manufacturers.GetAll();
            ViewData["Products"] = _products.GetAllNew();
            ViewData["Laboratories"] = _laboratories.GetAll();
            ViewData["Clients"] = _clients.GetAll();
            return View();
        }

        // POST: Projects/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(int manufactureId, int clientId, int laboratoryId, bool NativeProject, [Bind("Id,BrandName,ProductType,ProjectCategory,CRSNumber,TargetDate,ProjectStatus,RegistrationNumber,SampleSubmissionDate,DateOfExpiry")] Product newProduct)
        {
            Manufacturer newProductManufacturer = _manufacturers.GetById(manufactureId);
            if (newProductManufacturer != null)
            {
                newProduct.Manufacturer = newProductManufacturer;
            }
            Laboratory newProductLab = _laboratories.GetById(laboratoryId);
            if (newProductLab != null)
            {
                newProduct.Laboratory = newProductLab;
            }
            Client newProductClient = _clients.GetById(clientId);
            ModelState.Remove("Laboratory");
            ModelState.Remove("Manufacturer");
            newProduct.NativeProject = NativeProject;
            if (ModelState.IsValid && newProductClient != null)
            {
                newProductClient.Products.Add(newProduct);
                _products.Add(newProduct, newProductClient);
                return RedirectToAction("AddModels",new { productId= newProduct.Id});
            }
            else
            {
                ViewData["Manufacturers"] = _manufacturers.GetAll();
                ViewData["Products"] = _products.GetAllNew();
                ViewData["Laboratories"] = _laboratories.GetAll();
                ViewData["Clients"] = _clients.GetAll();
                return View();
            }
        }
        // GET: Models/Create
        public IActionResult AddModels(int productId)
        {
            var product = _products.GetById(productId);
            if (product == null)
            {
                return RedirectToAction("Index", "Projects");
            }
            return View(product);
        }

        // POST: Models/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<string> AddModelsConfirm(int ProductId, string Modeldata)
        {
            if (Modeldata != null && ProductId != null)
            {
                Product currentproduct = _products.GetById(ProductId);
                currentproduct.Models = Modeldata;
                _products.Update(currentproduct);
                return "Successfully Updated";

            }
            return "Some error";
        }

        // GET: Projects/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = _products.GetById(id);
            if (product == null)
            {
                return NotFound();
            }
            ViewData["Manufacturers"] = _manufacturers.GetAll();
            ViewData["Products"] = _products.GetAllNew();
            ViewData["Laboratories"] = _laboratories.GetAll();
            ViewData["Clients"] = _clients.GetAll();
            return View(product);
        }

        // POST: Projects/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, int manufactureId, int laboratoryId, [Bind("Id,BrandName,ProductType,ProjectCategory,CRSNumber,TargetDate,ProjectStatus,RegistrationNumber,SampleSubmissionDate,Models")] Product product)
        {
            bool state;
            if (id != product.Id)
            {
                return NotFound();
            }
            Manufacturer newProductManufacturer = _manufacturers.GetById(manufactureId);
            if (newProductManufacturer != null)
            {
                product.Manufacturer = newProductManufacturer;
            }

            Laboratory newProductLab = _laboratories.GetById(laboratoryId);
            if (newProductLab != null)
            {
                product.Laboratory = newProductLab;
            }
            state = _products.Update(product);
            if (state == true)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View(product);
            }
        }
    }

    // GET: Projects/Delete/5
    //public async Task<IActionResult> Delete(int? id)
    //{
    //    if (id == null)
    //    {
    //        return NotFound();
    //    }

    //    var product = await _context.Products
    //        .SingleOrDefaultAsync(m => m.Id == id);
    //    if (product == null)
    //    {
    //        return NotFound();
    //    }

    //    return View(product);
    //}

    //// POST: Projects/Delete/5
    //[HttpPost, ActionName("Delete")]
    //[ValidateAntiForgeryToken]
    //public async Task<IActionResult> DeleteConfirmed(int id)
    //{
    //    var product = await _context.Products.SingleOrDefaultAsync(m => m.Id == id);
    //    _context.Products.Remove(product);
    //    await _context.SaveChangesAsync();
    //    return RedirectToAction("Index");
    //}

    //private bool ProductExists(int id)
    //{
    //    return _context.Products.Any(e => e.Id == id);
    //}
}

