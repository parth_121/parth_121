using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SHTData;
using SHTData.Models;

namespace ShannonTech.Controllers
{
    public class LaboratoriesController : BaseController
    {
        private ILaboratory _laboratories;

        public LaboratoriesController(ILaboratory laboratories)
        {
            _laboratories = laboratories;
        }

        // GET: Laboratories
        public IActionResult Index()
        {
            return View(_laboratories.GetAll());
        }

        // GET: Laboratories/Details/5
        public IActionResult Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var laboratory = _laboratories.GetById(id);

            if (laboratory == null)
            {
                return NotFound();
            }

            return View(laboratory);
        }

        // GET: Laboratories/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Laboratories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id,Name,Location,ContactPersonName,ContactPersonPhoneNumber")] Laboratory laboratory)
        {
            if (ModelState.IsValid)
            {
                _laboratories.Add(laboratory);
                return RedirectToAction("Index");
            }
            return View(laboratory);
        }

        // GET: Laboratories/Edit/5
        public IActionResult Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var laboratory = _laboratories.GetById(id);
            if (laboratory == null)
            {
                return NotFound();
            }
            return View(laboratory);
        }

        // POST: Laboratories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,Name,Location,ContactPersonName,ContactPersonPhoneNumber")] Laboratory laboratory)
        {
            if (id != laboratory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _laboratories.Update(laboratory);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LaboratoryExists(laboratory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(laboratory);
        }

        private bool LaboratoryExists(int id)
        {
            return _laboratories.GetAll().Any(e => e.Id == id);
        }

        // GET: Laboratories/Delete/5
        //public IActionResult Delete(int id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var laboratory = _laboratories.GetById(id);
        //    if (laboratory == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(laboratory);
        //}

        //// POST: Laboratories/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var laboratory = await _context.Laboratories.SingleOrDefaultAsync(m => m.Id == id);
        //    _context.Laboratories.Remove(laboratory);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}
    }
}
