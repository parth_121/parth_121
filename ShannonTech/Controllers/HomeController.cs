using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ShannonTech.Controllers
{
    public class HomeController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            if (username == "rkt" && password == "rhythm89")
            {
                HttpContext.Session.SetString("rkt", "Logged in");
                return RedirectToAction("Dashboard");
            }
            return RedirectToAction("Index");
        }
        public IActionResult LogOut()
        {
            HttpContext.Session.Remove("rkt");
            return RedirectToAction("Index");
        }
        public IActionResult Dashboard()
        {
            return View();
        }
    }
}