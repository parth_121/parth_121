using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShannonTech.Models;
using SHTData;
using SHTData.Models;

namespace ShannonTech.Controllers
{
    public class ManufacturersController : BaseController
    {
        private IManufacturer _manufacturers;
        private IProduct _products;

        public ManufacturersController(IManufacturer manufacturers, IProduct products)
        {
            _products = products;
            _manufacturers = manufacturers;
        }

        // GET: Manufacturers
        public IActionResult Index()
        {
            return View(_manufacturers.GetAll());
        }

        // GET: Manufacturers/Details/5
        public IActionResult Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var manufacturer = _manufacturers.GetById(id);
            var manufacturerProducts = _products.GetByManufacturerId(id);
            if (manufacturer == null)
            {
                return NotFound();
            }
            ViewData["ManufacturerProducts"] = manufacturerProducts;
            return View(manufacturer);
        }

        // GET: Manufacturers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Manufacturers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ManufacturerViewModel manufacturerViewModel)
        {
            if (ModelState.IsValid)
            {
                Manufacturer manufacturer = new Manufacturer
                {
                    Name = manufacturerViewModel.Name,
                    BrandName = manufacturerViewModel.BrandName,
                    Location = manufacturerViewModel.Location,
                    LoginId = manufacturerViewModel.LoginId,
                    LoginPassword = manufacturerViewModel.LoginPassword,
                    ContantName = manufacturerViewModel.ContantName,
                    ContactEmail = manufacturerViewModel.ContactEmail,
                    AIRName = manufacturerViewModel.AIRName,
                    AIRPhoneNumber = manufacturerViewModel.AIRPhoneNumber,
                    AIREmail = manufacturerViewModel.AIREmail,
                    AIRDesignation = manufacturerViewModel.AIRDesignation,
                    AIRCompany = manufacturerViewModel.AIRCompany,
                    AIRCompanyAddress=manufacturerViewModel.AIRCompanyAddress
                };
                _manufacturers.Add(manufacturer);
                return RedirectToAction("Index");
            }
            return View(manufacturerViewModel);
        }

        // GET: Manufacturers/Edit/5
        public IActionResult Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var manufacturer = _manufacturers.GetById(id);
            if (manufacturer == null)
            {
                return NotFound();
            }
            return View(manufacturer);
        }

        // POST: Manufacturers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,Name,BrandName,Location,LoginId,LoginPassword,ContantName,ContactEmail,AIRName,AIRPhoneNumber,AIREmail,AIRDesignation,AIRCompany")] Manufacturer manufacturer)
        {
            if (id != manufacturer.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _manufacturers.Update(manufacturer);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ManufacturerExists(manufacturer.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(manufacturer);
        }

        private bool ManufacturerExists(int id)
        {
            return _manufacturers.GetAll().Any(e => e.Id == id);
        }

        //// GET: Manufacturers/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var manufacturer = await _context.Manufacturers
        //        .SingleOrDefaultAsync(m => m.Id == id);
        //    if (manufacturer == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(manufacturer);
        //}

        //// POST: Manufacturers/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var manufacturer = await _context.Manufacturers.SingleOrDefaultAsync(m => m.Id == id);
        //    _context.Manufacturers.Remove(manufacturer);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}


    }
}
