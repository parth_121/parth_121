using Microsoft.AspNetCore.Mvc;
using ShannonTech.Models;
using SHTData;
using SHTData.Models;

namespace ShannonTech.Controllers
{
    public class ClientsController : BaseController
    {
        private IProduct _products;
        private IClient _clients;

        public ClientsController(IClient clients, IProduct products)
        {
            _products = products;
            _clients = clients;
        }

        // GET: Clients
        public IActionResult Index()
        {
            return View(_clients.GetAll());
        }

        // GET: Clients/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var client = _clients.GetById(id);
            if (client == null)
            {
                return NotFound();
            }
            return View(client);
        }

        // GET: Clients/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Clients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ClientViewModel clientViewModel)
        {
            if (ModelState.IsValid)
            {
                Client client = new Client
                {
                    Email = clientViewModel.Email,
                    Name = clientViewModel.Name,
                    Designation = clientViewModel.Designation,
                    Company = clientViewModel.Company
                };
                _clients.Add(client);
                return RedirectToAction("Index");
            }
            return View(clientViewModel);
        }

        // GET: Clients/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var client = _clients.GetById(id);
        //    if (client == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(client);
        //}
        //
        //// POST: Clients/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("Id,Name,PhoneNumber,Email,Designation,Company")] Client client)
        //{
        //    if (id != client.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(client);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!ClientExists(client.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction("Index");
        //    }
        //    return View(client);
        //}

        //// GET: Clients/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var client = await _context.Clients
        //        .SingleOrDefaultAsync(m => m.Id == id);
        //    if (client == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(client);
        //}

        //// POST: Clients/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var client = await _context.Clients.SingleOrDefaultAsync(m => m.Id == id);
        //    _context.Clients.Remove(client);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        //private bool ClientExists(int id)
        //{
        //    return _context.Clients.Any(e => e.Id == id);
        //}
    }
}
