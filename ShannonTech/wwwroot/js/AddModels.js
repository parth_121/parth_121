﻿// ARRAY FOR HEADER.
var arrHead = new Array();
arrHead = ['Options', 'Model Numbers(Comma seprated)', 'Test Report Number', 'Test Report Received Date', 'Description'];      // SIMPLY ADD OR REMOVE VALUES IN THE ARRAY FOR TABLE HEADERS.

// FIRST CREATE A TABLE STRUCTURE BY ADDING A FEW HEADERS AND
// ADD THE TABLE TO YOUR WEB PAGE.
function createTable() {
    var empTable = document.createElement('table');
    empTable.setAttribute('id', 'empTable');            // SET THE TABLE ID.

    var tr = empTable.insertRow(-1);

    for (var h = 0; h < arrHead.length; h++) {
        var th = document.createElement('th');          // TABLE HEADER.
        th.innerHTML = arrHead[h];
        tr.appendChild(th);
    }

    var div = document.getElementById('cont');
    div.appendChild(empTable);    // ADD THE TABLE TO YOUR WEB PAGE.
}

// ADD A NEW ROW TO THE TABLE.s
function addRow() {
    var empTab = document.getElementById('empTable');

    var rowCnt = empTab.rows.length;        // GET TABLE ROW COUNT.
    var tr = empTab.insertRow(rowCnt);      // TABLE ROW.
    tr = empTab.insertRow(rowCnt);

    for (var c = 0; c < arrHead.length; c++) {
        var td = document.createElement('td');          // TABLE DEFINITION.
        td = tr.insertCell(c);

        if (c == 0) {           // FIRST COLUMN.
            // ADD A BUTTON.
            var button = document.createElement('input');

            // SET INPUT ATTRIBUTE.
            button.setAttribute('type', 'button');
            button.setAttribute('value', 'Remove');

            // ADD THE BUTTON's 'onclick' EVENT.
            button.setAttribute('onclick', 'removeRow(this)');

            td.appendChild(button);
        }
        else if (c == 3) {
            // CREATE AND ADD DATE AND IN EACH CELL.
            var ele = document.createElement('input');
            ele.setAttribute('class', 'form-control date');
            ele.setAttribute('type', 'date');
            ele.setAttribute('placeholder', 'Ex: 30/07/2016');
            ele.setAttribute('data-val', 'true');
            ele.setAttribute('type', 'date');
            ele.setAttribute('data-val-required', 'The TargetDate field is required.');
            ele.setAttribute('type', 'date');
            ele.setAttribute('value', '');
            ele.setAttribute('required', 'true');



            td.appendChild(ele);
        }
        else if (c == 1) {
            // CREATE AND ADD TEXTBOX IN EACH CELL.
            var ele = document.createElement('input');
            ele.setAttribute('type', 'text');
            ele.setAttribute('class', 'form-control');
            ele.setAttribute('value', '');

            td.appendChild(ele);
        }
        else {
            // CREATE AND ADD TEXTBOX IN EACH CELL.
            var ele = document.createElement('input');
            ele.setAttribute('type', 'text');
            ele.setAttribute('class', 'form-control');
            ele.setAttribute('value', '');

            td.appendChild(ele);
        }
    }
}

// ADD A Database ROW TO THE TABLE.s
function addDataRows(data) {
    for (var i = 1; i <= Object.keys(data).length; i++) {
        addEditRow(data[i]);
    }
}
function addEditRow(data) {
    var empTab = document.getElementById('empTable');

    var rowCnt = empTab.rows.length;        // GET TABLE ROW COUNT.
    var tr = empTab.insertRow(rowCnt);      // TABLE ROW.
    tr = empTab.insertRow(rowCnt);

    for (var c = 0; c < arrHead.length; c++) {
        var td = document.createElement('td');          // TABLE DEFINITION.
        td = tr.insertCell(c);

        if (c == 0) {           // FIRST COLUMN.
            // ADD A BUTTON.
            var button = document.createElement('input');

            // SET INPUT ATTRIBUTE.
            button.setAttribute('type', 'button');
            button.setAttribute('value', 'Remove');

            // ADD THE BUTTON's 'onclick' EVENT.
            button.setAttribute('onclick', 'removeRow(this)');

            td.appendChild(button);
        }
        else if (c == 3) {
            // CREATE AND ADD DATE AND IN EACH CELL.
            var ele = document.createElement('input');
            ele.setAttribute('class', 'form-control date');
            ele.setAttribute('type', 'date');
            ele.setAttribute('placeholder', 'Ex: 30/07/2016');
            ele.setAttribute('data-val', 'true');
            ele.setAttribute('type', 'date');
            ele.setAttribute('data-val-required', 'The TargetDate field is required.');
            ele.setAttribute('type', 'date');
            ele.setAttribute('value', data.TestReportReceivedDate);
            ele.setAttribute('required', 'true');



            td.appendChild(ele);
        }
        else if (c == 1) {
            // CREATE AND ADD TEXTBOX IN EACH CELL.
            var ele = document.createElement('input');
            ele.setAttribute('type', 'text');
            ele.setAttribute('class', 'form-control');
            ele.setAttribute('value', data.ModelNumbers);

            td.appendChild(ele);
        }
        else if (c == 2) {
            // CREATE AND ADD TEXTBOX IN EACH CELL.
            var ele = document.createElement('input');
            ele.setAttribute('type', 'text');
            ele.setAttribute('class', 'form-control');
            ele.setAttribute('value', data.TestReportNumber);

            td.appendChild(ele);
        }
        else if (c == 4) {
            // CREATE AND ADD TEXTBOX IN EACH CELL.
            var ele = document.createElement('input');
            ele.setAttribute('type', 'text');
            ele.setAttribute('class', 'form-control');
            ele.setAttribute('value', data.Description);

            td.appendChild(ele);
        }
    }
}
// DELETE TABLE ROW.
function removeRow(oButton) {
    var empTab = document.getElementById('empTable');
    empTab.deleteRow(oButton.parentNode.parentNode.rowIndex);       // BUTTON -> TD -> TR.
}

// EXTRACT AND SUBMIT TABLE DATA.
function sumbit() {
    var myTab = document.getElementById('empTable');
    var Data = {};
    var count = 1;


    // LOOP THROUGH EACH ROW OF THE TABLE.
    for (row = 1; row < myTab.rows.length - 1; row++) {
        var singleModel = new Object();
        for (c = 0; c < myTab.rows[row].cells.length; c++) {   // EACH CELL IN A ROW.
            var element = myTab.rows.item(row).cells[c];
            if (c == 1) {
                if (element.childNodes[0].getAttribute('type') == 'text') {
                    singleModel.ModelNumbers = element.childNodes[0].value;
                }
            }
            if (c == 2) {
                if (element.childNodes[0].getAttribute('type') == 'text') {
                    singleModel.TestReportNumber = element.childNodes[0].value;
                }
            }
            if (c == 3) {
                if (element.childNodes[0].getAttribute('type') == 'date') {
                    singleModel.TestReportReceivedDate = element.childNodes[0].value;
                }
            }
            if (c == 4) {
                if (element.childNodes[0].getAttribute('type') == 'text') {
                    singleModel.Description = element.childNodes[0].value;
                }
            }
        }
        if (!(jQuery.isEmptyObject(singleModel))) {
            Data["" + count] = singleModel;
            count = count + 1;
        }
    }
    var currentProductId = $('#CurrentProductId').attr("value");
    console.log(JSON.stringify(Data));

    $.ajax({
        url: "/Projects/AddModelsConfirm",
        type: "POST",
        data: {
            ProductId: currentProductId,
            Modeldata: JSON.stringify(Data)
        },
        success: function (data) {
            alert(data);
            window.location.href("/Projects/Index")
        }
    }).done(function (response) {
        console.log(response)
    });
}