﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShannonTech.Models
{
    public class ClientViewModel
    {
        public int Id { get; set; }
        [Required]
        public String Name { get; set; }
        public String PhoneNumber { get; set; }
        [Required]
        [EmailAddress]
        public String Email { get; set; }
        [Required]
        public String Designation { get; set; }
        [Required]
        public String Company { get; set; }
    }
}
