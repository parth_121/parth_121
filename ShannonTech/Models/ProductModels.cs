﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShannonTech.Models
{
    public class ProductModels
    {
        public int Id { get; set; }
        public string ModelNumbers { get; set; }
        public string TestReportNumber { get; set; }
        public DateTime TestReportReceivedDate { get; set; }
        public string Description { get; set; }
    }
}
