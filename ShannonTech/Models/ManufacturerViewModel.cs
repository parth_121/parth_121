﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShannonTech.Models
{
    public class ManufacturerViewModel
    {
        public int Id { get; set; }
        [Required]
        public String Name { get; set; }
        [Required]
        public String BrandName { get; set; }
        [Required]
        public String Location { get; set; }
        public String LoginId { get; set; }
        public String LoginPassword { get; set; }
        [Required]
        public String ContantName { get; set; }
        [Required]
        [EmailAddress]
        public String ContactEmail { get; set; }
        [Required]
        public String AIRName { get; set; }
        public String AIRPhoneNumber { get; set; }
        [Required]
        [EmailAddress]
        public String AIREmail { get; set; }
        [Required]
        public String AIRDesignation { get; set; }
        [Required]
        public String AIRCompany { get; set; }
        [Required]
        public String AIRCompanyAddress { get; set; }
    }
}
