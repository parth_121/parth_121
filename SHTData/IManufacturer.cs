﻿using SHTData.Models;
using System.Collections.Generic;

namespace SHTData
{
    public interface IManufacturer
    {
        IEnumerable<Manufacturer> GetAll();
        Manufacturer GetById(int id);
        void Add(Manufacturer newManufacturer);
        bool Update(Manufacturer editManufacturer);
    }
}
