﻿using SHTData.Models;
using System.Collections.Generic;

namespace SHTData
{
    public  interface ILaboratory
    {
        IEnumerable<Laboratory> GetAll();
        Laboratory GetById(int id);
        void Add(Laboratory newLaboratory);
        bool Update(Laboratory editLaboratory);
    }
}
