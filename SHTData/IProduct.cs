﻿using SHTData.Models;
using System.Collections.Generic;

namespace SHTData
{
    public interface IProduct
    {
        IEnumerable<Product> GetAll();
        IEnumerable<Product> GetAllNew();
        Product GetById(int? id);
        bool Add(Product newProduct, Client client);
        bool Update(Product editProduct);
        IEnumerable<Product> GetByManufacturerId(int id);
    }
}
