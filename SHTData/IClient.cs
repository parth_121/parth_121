﻿using SHTData.Models;
using System.Collections.Generic;

namespace SHTData
{
    public interface IClient
    {
        IEnumerable<Client> GetAll();
        Client GetById(int? id);
        void Add(Client newClient);
    }
}
