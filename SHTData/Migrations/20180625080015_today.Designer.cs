﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using SHTData;

namespace SHTData.Migrations
{
    [DbContext(typeof(SHTContext))]
    [Migration("20180625080015_today")]
    partial class today
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.5")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("SHTData.Models.Client", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Company")
                        .IsRequired();

                    b.Property<string>("Designation")
                        .IsRequired();

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("PhoneNumber")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Clients");
                });

            modelBuilder.Entity("SHTData.Models.Laboratory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ContactPersonName")
                        .IsRequired();

                    b.Property<string>("ContactPersonPhoneNumber")
                        .IsRequired();

                    b.Property<string>("Location");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Laboratories");
                });

            modelBuilder.Entity("SHTData.Models.Manufacturer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AIRCompany")
                        .IsRequired();

                    b.Property<string>("AIRDesignation")
                        .IsRequired();

                    b.Property<string>("AIREmail")
                        .IsRequired();

                    b.Property<string>("AIRName")
                        .IsRequired();

                    b.Property<string>("AIRPhoneNumber")
                        .IsRequired();

                    b.Property<string>("ContactPhoneNumber");

                    b.Property<string>("ContantName");

                    b.Property<string>("Location")
                        .IsRequired();

                    b.Property<string>("LoginId");

                    b.Property<string>("LoginPassword");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Manufacturers");
                });

            modelBuilder.Entity("SHTData.Models.Model", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("ModelNumber")
                        .IsRequired();

                    b.Property<int?>("ProductId");

                    b.Property<string>("TestReportNumber");

                    b.Property<DateTime>("TestReportReceivedDate");

                    b.HasKey("Id");

                    b.HasIndex("ProductId");

                    b.ToTable("Models");
                });

            modelBuilder.Entity("SHTData.Models.Product", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BrandName")
                        .IsRequired();

                    b.Property<string>("CRSNumber");

                    b.Property<int?>("ClientId");

                    b.Property<int>("LaboratoryId");

                    b.Property<int>("ManufacturerId");

                    b.Property<string>("ProductType")
                        .IsRequired();

                    b.Property<string>("ProjectCategory")
                        .IsRequired();

                    b.Property<string>("ProjectStatus")
                        .IsRequired();

                    b.Property<string>("RegistrationNumber");

                    b.Property<DateTime>("SampleSubmissionDate");

                    b.Property<DateTime>("TargetDate");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.HasIndex("LaboratoryId");

                    b.HasIndex("ManufacturerId");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("SHTData.Models.Model", b =>
                {
                    b.HasOne("SHTData.Models.Product")
                        .WithMany("Models")
                        .HasForeignKey("ProductId");
                });

            modelBuilder.Entity("SHTData.Models.Product", b =>
                {
                    b.HasOne("SHTData.Models.Client")
                        .WithMany("Products")
                        .HasForeignKey("ClientId");

                    b.HasOne("SHTData.Models.Laboratory", "Laboratory")
                        .WithMany()
                        .HasForeignKey("LaboratoryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("SHTData.Models.Manufacturer", "Manufacturer")
                        .WithMany()
                        .HasForeignKey("ManufacturerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
