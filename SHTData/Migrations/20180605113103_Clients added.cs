﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SHTData.Migrations
{
    public partial class Clientsadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AIRId",
                table: "Products",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "AIRs",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Company = table.Column<string>(nullable: false),
                    Designation = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_AIRId",
                table: "Products",
                column: "AIRId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_AIRs_AIRId",
                table: "Products",
                column: "AIRId",
                principalTable: "AIRs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_AIRs_AIRId",
                table: "Products");

            migrationBuilder.DropTable(
                name: "Clients");

            migrationBuilder.DropIndex(
                name: "IX_Products_AIRId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "AIRId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "AIRs");
        }
    }
}
