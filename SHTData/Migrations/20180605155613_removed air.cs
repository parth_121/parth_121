﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SHTData.Migrations
{
    public partial class removedair : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manufacturers_AIRs_AIRId",
                table: "Manufacturers");

            migrationBuilder.DropTable(
                name: "AIRs");

            migrationBuilder.DropIndex(
                name: "IX_Manufacturers_AIRId",
                table: "Manufacturers");

            migrationBuilder.DropColumn(
                name: "AIRId",
                table: "Manufacturers");

            migrationBuilder.AddColumn<string>(
                name: "AIRCompany",
                table: "Manufacturers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AIRDesignation",
                table: "Manufacturers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AIREmail",
                table: "Manufacturers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AIRName",
                table: "Manufacturers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AIRPhoneNumber",
                table: "Manufacturers",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AIRCompany",
                table: "Manufacturers");

            migrationBuilder.DropColumn(
                name: "AIRDesignation",
                table: "Manufacturers");

            migrationBuilder.DropColumn(
                name: "AIREmail",
                table: "Manufacturers");

            migrationBuilder.DropColumn(
                name: "AIRName",
                table: "Manufacturers");

            migrationBuilder.DropColumn(
                name: "AIRPhoneNumber",
                table: "Manufacturers");

            migrationBuilder.AddColumn<int>(
                name: "AIRId",
                table: "Manufacturers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AIRs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Company = table.Column<string>(nullable: false),
                    Designation = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AIRs", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Manufacturers_AIRId",
                table: "Manufacturers",
                column: "AIRId");

            migrationBuilder.AddForeignKey(
                name: "FK_Manufacturers_AIRs_AIRId",
                table: "Manufacturers",
                column: "AIRId",
                principalTable: "AIRs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
