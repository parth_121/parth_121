﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHTData.Migrations
{
    public partial class AZURENEW : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CompanyName",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "TestReportNumber",
                table: "Products");

            migrationBuilder.RenameColumn(
                name: "ProjectCategory",
                table: "Products",
                newName: "BrandName");

            migrationBuilder.AddColumn<string>(
                name: "TestReportNumber",
                table: "Models",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TestReportNumber",
                table: "Models");

            migrationBuilder.RenameColumn(
                name: "BrandName",
                table: "Products",
                newName: "ProjectCategory");

            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                table: "Products",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "TestReportNumber",
                table: "Products",
                nullable: true);
        }
    }
}
