﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SHTData.Migrations
{
    public partial class Clientsaddedagain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_AIRs_AIRId",
                table: "Products");

            migrationBuilder.RenameColumn(
                name: "AIRId",
                table: "Products",
                newName: "ClientId");

            migrationBuilder.RenameIndex(
                name: "IX_Products_AIRId",
                table: "Products",
                newName: "IX_Products_ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Clients_ClientId",
                table: "Products",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Clients_ClientId",
                table: "Products");

            migrationBuilder.RenameColumn(
                name: "ClientId",
                table: "Products",
                newName: "AIRId");

            migrationBuilder.RenameIndex(
                name: "IX_Products_ClientId",
                table: "Products",
                newName: "IX_Products_AIRId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_AIRs_AIRId",
                table: "Products",
                column: "AIRId",
                principalTable: "AIRs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
