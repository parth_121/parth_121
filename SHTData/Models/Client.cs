﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SHTData.Models
{
    public class Client
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String PhoneNumber { get; set; }
        public String Email { get; set; }
        public String Designation { get; set; }
        public String Company { get; set; }

        public virtual ICollection<Product> Products { get; set; }

    }
}
