﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SHTData.Models
{
    public class Product
    {
        public int Id { get; set; }
        public String ProductType { get; set; }
        public Boolean NativeProject { get; set; }
        public String ProjectCategory { get; set; }
        public String CRSNumber { get; set; }
        public DateTime TargetDate { get; set; }
        public String ProjectStatus { get; set; }
        public String StatusOfRegistration { get; set; }
        public String RegistrationNumber { get; set; }
        public DateTime SampleSubmissionDate { get; set; }
        public DateTime DateOfExpiry { get; set; }
        public String Models { get; set; }


        public virtual Manufacturer Manufacturer { get; set; }
        public virtual Laboratory Laboratory { get; set; }

        //public virtual ICollection<Model> Models { get; set; }
    }
}
