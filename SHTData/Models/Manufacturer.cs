﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SHTData.Models
{
    public class Manufacturer
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String BrandName { get; set; }
        public String Location { get; set; }
        public String LoginId { get; set; }
        public String LoginPassword { get; set; }
        public String ContantName { get; set; }
        public String ContactEmail { get; set; }
        public String AIRName { get; set; }
        public String AIRPhoneNumber { get; set; }
        public String AIREmail { get; set; }
        public String AIRDesignation { get; set; }
        public String AIRCompany { get; set; }
        public String AIRCompanyAddress { get; set; }
    }
}
