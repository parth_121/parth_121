﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SHTData.Models
{
    public class Laboratory
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Location { get; set; }
        public String ContactPersonName { get; set; }
        public String ContactPersonPhoneNumber { get; set; }
    }
}
